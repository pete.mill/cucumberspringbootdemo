# CucumberSpringBootDemo

Illustrate a variety of techniques for using Spring Boot and Cucumber together

Use Cucumber expressions throughout  
Use Java8 lambdas for Step Definitions  
Use Spring for Dependency Injection  
... using annotations `@Autowired, @Value and @PropertySource`  
Use Spring Profiles for externalised dependencies (i.e. dealing with different target environments)  

