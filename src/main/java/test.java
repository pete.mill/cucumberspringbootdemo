
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

//    http://tutorials.jenkov.com/java-json/jackson-jsonnode.html
//    https://www.baeldung.com/jackson-json-node-tree-model

@Slf4j
public class JacksonJsonNodeTest {

    //JSON parsing and manipulation

    // read a value
    // change a value
    // delete a property
    // add a property
    // add an element to an array

    //    String jsonString = "{\"simple\":\"value\",\"data\":{\"translations\":[{\"translatedText\":\"Ariba\"},{\"translatedText\":\"Ariba\"}]},\"compound\":{\"c1\":\"compound one\",\"c2\":\"compound one\"}}\n";
    String jsonString = "{\"river\":{\"deep\":{\"mountain\":{\"high\":{\"summit\":\"Nanga Parbat\",\"andArrays\": [\"just\",\"as easy\",{\"toInfinity\": \"and Beyond\"}]}}}}}\";\n";

    // expensive so just have the one here
    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void testJackson() throws JsonProcessingException {

        JsonNode jsonNode = objectMapper.readTree(jsonString);

        String json = objectMapper.writeValueAsString(jsonNode);
//        System.out.println(json);

        String simples = jsonNode.get("simple").toString();
//        System.out.println(simples);

//        JsonNode is immutable.
//        If we want something to manipulate, use an ObjectNode
        ObjectNode rootNode = objectMapper.createObjectNode();
        System.out.printf("ObjectNode rootNode \n%s\n", rootNode.toString());

        //  copy the json node
        rootNode = jsonNode.deepCopy();
        System.out.printf("copied jsonNode \n%s\n", rootNode.toString());

        // change a value
        rootNode.put("simple", "new simple value");
        System.out.printf("changed \n%s\n", rootNode.toString());

        rootNode.remove("simple");
        System.out.printf("removed \n%s\n", rootNode.toString());

        rootNode.put("simple", "replaced");
        System.out.printf("added again \n%s\n", rootNode.toString());

        // read simple
        System.out.printf("simple: %s\n", rootNode.get("simple").toString());
        // read compound
        System.out.printf("get compound \n%s\n", rootNode.path("compound").get("c1").toString());
        System.out.printf("compound with array \n%s\n", rootNode
                .path("data")
                .path("translations").path(1)
                .get("translatedText").toString());

    }

    @Test
    void testJsonPointerReads() throws JsonProcessingException {
//        JSON Pointer is a string syntax for identifying a specific value within a JSON document, defined by the RFC 6901.
//        String jsonString = """
//{"river":{"deep":{"mountain":{"high":{"summit":"at last","andArrays": ["just","as easy",{"toInfinity": "and Beyond"}]}}}}}";
//"""

        JsonNode jsonNode = objectMapper.readTree(jsonString);
        System.out.printf(" jsonNode array: %s\n", jsonNode.at("/river/deep/mountain/high").toString());

        ObjectNode rootNode = objectMapper.createObjectNode();
        rootNode = jsonNode.deepCopy();
        System.out.println(rootNode.toString());

        System.out.printf("summit: %s\n", rootNode.at("/river/deep/mountain/high").get("summit").toString());
        System.out.printf("also summit: %s\n", rootNode.at("/river/deep/mountain/high/summit").toString());
        System.out.printf("array: %s\n", rootNode.at("/river/deep/mountain/high/andArrays").toString());
        System.out.printf("array: %s\n", rootNode.at("/river/deep/mountain/high/andArrays/0").toString());
        System.out.printf("array: %s\n", rootNode.at("/river/deep/mountain/high/andArrays/1").toString());
        System.out.printf("toInfinity: %s\n", rootNode.at("/river/deep/mountain/high/andArrays/2/toInfinity").toString());

        // could also put, remove etc this way - BUT it's clunky
        // you have to use at which gets you a JSON node which you then have to cast to an ObjectNode
        // which you assign to something so that you have an ObjectNode to work on
        ObjectNode newOne = (ObjectNode) rootNode.at("/river/deep/mountain/high");
        newOne.put("summit", "K2");
        System.out.printf("changed summit: %s\n", rootNode.at("/river/deep/mountain/high").get("summit").toString());

        // or cast inline
        System.out.printf("changed inline: %s\n"
                , ((ObjectNode) rootNode.at("/river/deep/mountain/high")).put("summit", "K2").get("summit").toString());

    }

    //    https://mkyong.com/java/jackson-tree-model-example/
    // CRUD operations
    @Test
    void testJsonPointerObjects() throws JsonProcessingException {

        JsonNode injectMe = objectMapper.readTree("{\"injected key\":\"snippet\"}");
//        ObjectNode rootNode = objectMapper.createObjectNode();
        ObjectNode rootNode = objectMapper.readTree(jsonString).deepCopy();

        // add a simple value to the root
        rootNode.put("top level key", "top level value");

        // add a JsonNode object to the root
        rootNode.set("top", injectMe);

        // get a 'pointer' to a Node within the tree
        ObjectNode insertAtNode = (ObjectNode) rootNode.at("/river/deep/mountain");

        // add a simple value to the node within the tree
        insertAtNode.put("injectedValue", "Simple Gifts");

        // add a JsonNode object to the node within the tree
        insertAtNode.set("injectedObject", injectMe);

        log.info("{}" , rootNode.toPrettyString());

    }

    @Test
    void testNestedObjects() throws JsonProcessingException {
        ObjectNode rootNode = objectMapper.createObjectNode();
        ObjectNode child = objectMapper.createObjectNode();
        ObjectNode grandchild = objectMapper.createObjectNode();

        // have to create these bottom-up
        JsonNode injectKey = objectMapper.readTree("{\"injected key\":\"injected value\"}");
        grandchild.set("grandchild", injectKey);
        child.set("child", grandchild);
        rootNode.set("toplevel", child);

        log.info("{}" , rootNode.toPrettyString());
    }

    @Test
    void testArrays(){

    }

}
