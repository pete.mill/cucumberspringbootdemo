# -- FILE: features/sample.feature
Feature: Simple API tests

  Requirements

  Configurable environment and test profile (e.g. data so can use same test profile but for different environments and vice versa)
  Read POST body from file - depending on test profile - either JSON or XML
  Amend JSON or XML body (using Jackson)

  GET/POST/PUT etc (using Rest Assured)

  Assert actual response body against expected - either read expected from file / amend with Jackson
    (using JSON-Unit? XML-Unit?)
  Full-body comparison or property level comparison ?

  How should we state this with Cucumber?  Surely covered by unit-tests?

  Scenario: Make a call to a REST API
    * read something


  Scenario Template: <Test Case> value
    * test "<value>"

    Examples:
      | Test Case            | value      |
      | too big              | 9999999999 |
      | too small            | 1          |
      | non-ascii characters | ££££££     |

    # or you could look up "testcase" in a properties file to get the value - you trip over the spaces though

  @runme
  Scenario: test a postcode
    Given lookup postcode "S11 8JJ"
    Then response matches this one for "S11 8JJ"
