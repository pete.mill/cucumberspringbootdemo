package uk.gov.dwp.idt.steps;

import io.cucumber.java8.En;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.response.ValidatableResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.core.env.Environment;

import static io.restassured.RestAssured.given;
import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;
import static uk.gov.dwp.idt.helpers.FileHelper.getExpectedResponse;

@Slf4j
public class SampleSteps implements En {

    RequestSpecBuilder requestSpecBuilder;
    ResponseSpecBuilder responseSpecBuilder;
    ValidatableResponse response;

    public SampleSteps(Environment env) {

        // request characteristics common to most tests in this class
        requestSpecBuilder = new RequestSpecBuilder()
                .setBaseUri(env.getProperty("host.postcodes"))
                .setAccept(ContentType.JSON);

        // response validation common to most tests in this class
        responseSpecBuilder = new ResponseSpecBuilder()
                .expectStatusCode(HttpStatus.SC_OK)
                .expectContentType(ContentType.JSON);

        Given("read something", () -> {
            log.info("***** {}", env.getProperty("test.profile"));

            /*
             * need a url and a path
             * path parameters - RA can replace these if you use {pathParam} ?
             * query parameters - RA also handles these
             * headers
             * body - possibly a template that's manipulated
             *
             * response object
             *
             * assertions on response
             * */
        });

        Given("test {string}", (String testValue) -> {
            log.info("test value {} ", testValue);
        });


        Given("lookup postcode {string}", (String postcode) -> {

            // use RestAssured as the http client
            // use response spec for common validation
            // keep the complete response for subsequent use
            response = given().spec(requestSpecBuilder.build())
                    .pathParam("postcode", postcode)
                    .when().request(Method.GET, env.getProperty("host.path"))
                    .then().spec(responseSpecBuilder.build());

        });

        Then("response matches this one for {string}", (String expectedResponseKey) -> {
            // do a full body comparison against file stored on classpath
            // use JSON-Unit to assert as a JSON object
            // this allows for some fine-grained control over the validation
            // and sensible reporting of differences on failure
            String expectedResponse = getExpectedResponse(env.getProperty("test.profile"), expectedResponseKey);

            // Using JSON-Unit needs an explicit dependency on GSON or Jackson adding to POM
            // Lets give it Jackson since we're not scared of it now and can use it for XML as well as JSON
            assertThatJson(response.extract().body().asString()).isEqualTo(expectedResponse);
        });
    }

}
