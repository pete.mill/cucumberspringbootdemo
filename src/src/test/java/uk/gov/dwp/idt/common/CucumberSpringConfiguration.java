package uk.gov.dwp.idt.common;

import io.cucumber.spring.CucumberContextConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.context.SpringBootTest;

@CucumberContextConfiguration
@SpringBootTest()
@Slf4j
public class CucumberSpringConfiguration {}
