package uk.gov.dwp.idt.testrunners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import lombok.extern.slf4j.Slf4j;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@Slf4j
@RunWith(Cucumber.class)
@CucumberOptions(
//        plugin = {"junit:output", "io.qameta.allure.cucumber6jvm.AllureCucumber6Jvm"}
        plugin = {"junit:output"}
        , features = "src/test/resources/features"
        , glue = { //must be full package names
        "uk.gov.dwp.idt.common"
        , "uk.gov.dwp.idt.steps"
}
        , tags = "@runme"
        , dryRun = false
)
public class TestRunner {
}
