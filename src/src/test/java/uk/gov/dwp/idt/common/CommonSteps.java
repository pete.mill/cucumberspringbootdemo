package uk.gov.dwp.idt.common;

import io.cucumber.java8.En;
import io.cucumber.java8.Scenario;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;

import java.util.Optional;

@Slf4j
public class CommonSteps implements En {

    public CommonSteps(Environment env) {

        Before("", (Scenario scenario) -> {
            log.info("********** Starting Scenario >>{}<<", scenario.getName());
//            log.info("Using test profile >>{}<<", env.getProperty("test.profile"));

            // If GSON was also loaded and you need to steer JSON-Unit towards Jackson
            //System.setProperty("json-unit.libraries", "jackson2");

            if (log.isDebugEnabled()) {
                RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
            }

        });

        After("", (Scenario scenario) -> {
            log.info("********** Ending Scenario >>{}<<", scenario.getName());
        });
    }
}
