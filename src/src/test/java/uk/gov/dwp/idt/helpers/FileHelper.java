package uk.gov.dwp.idt.helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class FileHelper {

    // not convinced that this is the way to provide these values but they'll do for now
    private static final String DOT = ".";
    private static final String PROFILES = "profiles/";
    private static final String RESPONSE_DATA = "/data/response/";
    private static final String REQUEST_DATA = "/data/request/";
    private static final String DEFAULT_EXTENSION = "json";

    public static String getExpectedResponse(String testProfile, String expectedResponseKey) throws IOException {

        String expectedResponsePath = PROFILES + testProfile + RESPONSE_DATA + normaliseFileName(expectedResponseKey) + DOT + DEFAULT_EXTENSION;
        return getResourceFileAsString(expectedResponsePath);

    }

    public static String getRequestBody(String testProfile, String requestKey) throws IOException {

        String expectedResponsePath = PROFILES + testProfile + REQUEST_DATA + normaliseFileName(requestKey) + DOT + DEFAULT_EXTENSION;
        return getResourceFileAsString(expectedResponsePath);

    }

    public static String getExpectedResponse(String testProfile, String expectedResponseKey, String extension) throws IOException {

        String expectedResponsePath = PROFILES + testProfile + RESPONSE_DATA + normaliseFileName(expectedResponseKey) + DOT + extension;
        return getResourceFileAsString(expectedResponsePath);

    }

    public static String getRequestBody(String testProfile, String requestKey, String extension) throws IOException {

        String expectedResponsePath = PROFILES + testProfile + REQUEST_DATA + normaliseFileName(requestKey) + DOT + extension;
        return getResourceFileAsString(expectedResponsePath);

    }

    /*
    Helper method to read a file from the classpath
     */
    private static String getResourceFileAsString(String fileName) throws IOException {
        try (InputStream inputStream = ClassLoader.getSystemClassLoader().getResourceAsStream(fileName)) {
            if (inputStream == null) {
                return null;
            } else {
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
                    return reader.lines().collect(Collectors.joining(System.lineSeparator()));
                }
            }
        }
    }

    private static String normaliseFileName(String filename) {
        return filename.replaceAll(" ", "_");
    }

}
