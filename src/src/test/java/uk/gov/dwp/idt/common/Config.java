package uk.gov.dwp.idt.common;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:/profiles/${test.profile}/test.properties", encoding = "UTF-8")
public class Config {
}
