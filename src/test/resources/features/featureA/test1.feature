Feature: Test 1

  Step definitions are in package common and in package stepsalpha in 2 separate step definitions A and B
  This Feature also demonstrates the DI capabilities and the use of different Spring Profiles


  Scenario: Simple test 1
    Given User can access Common Step defs
    And User can access step defs A
    And User can access step defs B

  @thisone
  Scenario: DI test
    Given User can use autowired class
    And User can use injected values
