package com.example.stepsalpha;

import io.cucumber.java8.En;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StepDefsB implements En {

    public StepDefsB() {
        Given("User can access step defs B", () -> {
//            System.out.println("User can access step defs B");
            log.info("User can access step defs B");
        });
    }

}
