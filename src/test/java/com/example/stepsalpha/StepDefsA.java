package com.example.stepsalpha;

import io.cucumber.java8.En;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StepDefsA implements En {

    public StepDefsA() {
        Given("User can access step defs A", () -> {
//            System.out.println("User can access step defs A");
            log.info("User can access step defs A");
        });
    }

}
