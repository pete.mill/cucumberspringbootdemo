package com.example.stepsbeta;

import io.cucumber.java8.En;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StepDefsD implements En {

    public StepDefsD() {

        Given("User can access step defs D", () -> {
            log.info("User can access step defs D");
        });

        Given("User can use {string} expressions", (String phrase) -> {
            log.info("String in quotes: {}", phrase);
        });

        Given("User can use {word} expressions", (String word) -> {
            log.info("Single word: {}", word);
        });

    }
}
