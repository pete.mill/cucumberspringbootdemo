package com.example.stepsbeta;

import io.cucumber.java8.En;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StepDefsC implements En {

    public StepDefsC() {
        Given("User can access step defs C", () -> {
//            System.out.println("User can access step defs C");
            log.info("User can access step defs C");
        });
    }
}
