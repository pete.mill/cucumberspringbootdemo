package com.example.common;

import io.cucumber.java8.En;
import io.cucumber.java8.Scenario;
import io.cucumber.spring.CucumberContextConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;

@Slf4j
public class StepDefsCommon implements En {

    @Autowired
    Client myClient;

    // this uses Spring Environment class
    @Autowired
    Environment environment;

    // this comes from the application yaml file
    @Value("${data}")
    String data;

    // this comes from the test.properties file for the active spring profile (could be default!)
    @Value("${environment:NotFound}")
    String env;

    public StepDefsCommon() {

        Before("", (Scenario scenario) -> {
            log.info("********** Starting Scenario {}", scenario.getName());
        });

        After("", (Scenario scenario) -> {
            log.info("********** Ending Scenario {}", scenario.getName());
        });

        Given("User can access Common Step defs", () -> {
            log.info("User can access Common Step defs");
        });

        Given("User can use autowired class", () -> {
            log.info("myClient says: {}", myClient.sayHello());

            log.info("DI works so here is base name: {}", myClient.getBaseName());
            log.info("baseUrl: {}", myClient.getBaseUrl());
            log.info("scheme:host:port {}:{}:{}", myClient.scheme, myClient.host, myClient.port);  //etc. etc.
        });

        Given("User can use injected values", () -> {
            log.info("Data in {}", data);
            log.info("environment value from Spring Environment: {}", environment.getProperty("environment"));
            log.info("environment value from @Value annotation: {}", env);
        });
    }
}
