package com.example.common;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:${data}/test.properties", encoding = "UTF-8")
public class Config {
    /*
    Property values can be injected directly using the @Value annotation
    or accessed via Spring’s Environment abstraction

    This config class exists just to be annotated as @Configuration
    It then loads dynamically the properties file set at @PropertySource
    ${data} should be set in the application properties file

     */
}
