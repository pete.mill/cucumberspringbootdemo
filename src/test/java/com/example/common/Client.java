package com.example.common;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

// this class is to be injected using Autowired
// this only works if it is in the same package as Config and CucumberSpringConfiguration

@Component
@Scope(SCOPE_CUCUMBER_GLUE)
@Getter
public class Client {

    @Value("${server.scheme}")
    String scheme;

    @Value("${server.host}")
    String host;

    @Value("${server.port}")
    String port;

    @Value("${server.scheme}://${server.host}:${server.port}")
    String baseUrl;

    private final String baseName = "MyBaseName has been autowired";

    public String sayHello(){
        return "Here I am";
    }

}
