package com.example.testrunners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = "junit:output",
        features = "src/test/resources/features/featureA",
        glue = { //must be full package names
                "com.example.common"
                , "com.example.stepsalpha"},
        dryRun = false,
        tags = "@thisone"
)
public class TestRunnerFeatureA {
}
