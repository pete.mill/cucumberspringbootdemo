package com.example.testrunners;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = "junit:output",
        features = "src/test/resources/features",
        glue = { //must be full package names
                "com.example.common"
                , "com.example.stepsalpha"
                , "com.example.stepsbeta"},
//        tags = "not @thisone",
        dryRun = false
)
public class TestRunnerAllFeatures {
}
